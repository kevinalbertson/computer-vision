function [normalized, means, vars] = Normalize(features, means, vars)
    % If the means and variances are not provided, they are computed on
    % this matrix.
    if (~exist('means', 'var'));
        means = mean(features, 1);
        vars = var(features, 1);
    end;
    
    % Normalize the feature matrix.
    deviations = sqrt(vars);
    
    normalized = features - repmat(means, length(features), 1);
    normalized = normalized ./ repmat(deviations, length(features), 1);