function experiments = GetExperiments()
    experiments = containers.Map();
    experiments('dilate') = 1;
    experiments('symmetry') = 1;
    experiments('topk') = 0;
    experiments('svm') = 1;
    experiments('component_count') = 0;
    experiments('dimension_ratio') = 0;
    experiments('normalized_centroid') = 1;
    
%TODO: attempt cell projection