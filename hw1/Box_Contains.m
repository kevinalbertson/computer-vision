function [contains] = Box_Contains(box, loc)
    contains = box(1,1) <= loc(1,1) && loc(1,1) <= box(1,2) && box(1,3) <= loc(1,2) && loc(1,2) <= box(1,4);