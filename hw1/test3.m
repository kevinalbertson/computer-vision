test3_locations = [
    66 84;63 189;65 265;62 365;63 440;52 531;54 611;
    148 82;142 187;146 266;143 363;142 447;137 547;132 606;
    217 83;215 197;210 277;208 364;205 446;202 542;199 610;
    278 85;271 200;269 270;270 362;260 440;250 530;255 605;
    348 82;338 195;338 261;331 361;326 442;318 535;316 598;
    427 76;415 198;407 266;409 353;395 430;385 530;379 601;
    534 71;513 192;510 261;504 344;498 427;489 510;488 585;
    622 76;616 203;615 272;605 354;606 423;598 513;594 585;
    696 66;690 192;690 263;678 350;672 420;669 519;661 577;
    768 52;760 180;761 258;757 335;742 414;737 522;725 576;
    860 54;853 169;840 247;829 333;818 415;802 513;794 577;
    948 54;937 157;922 238;907 334;906 407;877 522;878 577;
    1043 47;1042 155;1023 249;1014 335;1008 405;995 492;993 570;
    1129 43;1119 157;1113 245;1109 330;1096 408;1084 489;1078 562;
    1218 42;1215 157;1199 245;1192 330;1178 411;1170 489;1157 560;
    1306 43;1299 164;1286 250;1273 347;1276 412;1260 490;1252 563;
];

test3_letters = [
    'a';'a';'a';'a';'a';'a';'a';
    'd';'d';'d';'d';'d';'d';'d';
    'm';'m';'m';'m';'m';'m';'m';
    'n';'n';'n';'n';'n';'n';'n';
    'o';'o';'o';'o';'o';'o';'o';
    'p';'p';'p';'p';'p';'p';'p';
    'q';'q';'q';'q';'q';'q';'q';   
    'r';'r';'r';'r';'r';'r';'r';
    'u';'u';'u';'u';'u';'u';'u';
    'w';'w';'w';'w';'w';'w';'w';
    'f';'f';'f';'f';'f';'f';'f';
    'h';'h';'h';'h';'h';'h';'h';
    'k';'k';'k';'k';'k';'k';'k';
    's';'s';'s';'s';'s';'s';'s';
    'x';'x';'x';'x';'x';'x';'x';
    'z';'z';'z';'z';'z';'z';'z';
];

test3_labels = [];
letters = GetCharLabels();
for i=1:length(test3_letters);
    num = find(letters == test3_letters(i));
    test3_labels = [test3_labels; num];
end;

RunMyOCRRecognition('images/test3.bmp', test3_locations, test3_labels);