% Extracts unnormalized features from a file.
function [features, boxes] = OCR_Extract_Features(showplots, filename)
    experiments = GetExperiments();
    boxes = [];
    features = [];
    
    if(~exist(filename, 'file'))
        fprintf('Could not find file %s\n', filename);
        return;
    end;
    
    im = imread(filename);
    th = 210;
    im2 = uint8(im < th);
    L = bwlabel(im2);
    if experiments('dilate');
        L = bwmorph(L, 'dilate', 2);
        L = bwmorph(L, 'erode', 2);
        L = bwlabel(L);
    end;
    [features, boxes] = BoundingBox(L, im2, showplots);
    
    