% Generates the trained model and labels from the image files.
function [features, labels] = OCR_Make_Model()
    letters = GetCharLabels();
    features = [];
    labels = [];
    for i=1:length(letters);
        filename = strcat('images/', letters(i), '.bmp');
        [letter_features, letter_boxes] = OCR_Extract_Features(0, filename);
        features = [features; letter_features];
        labels = [labels; ones(length(letter_features),1) * i];
    end;
    