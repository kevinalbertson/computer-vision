function [result] = RunMyOCRRecognition(filename, locations, classes)
    experiments = GetExperiments();
    show_train_images = 0;
    letters = GetCharLabels();
    
    % Make the trained model.
    [train_features, train_labels] = OCR_Make_Model();
    [train_features, means, vars] = Normalize(train_features);
    
    % Extract the features from the test data.
    [test_features, test_boxes] = OCR_Extract_Features(1, filename);
    test_labels = ones(length(test_features),1);

    % Normalize the test features with the training means/variances.
    [test_features] = Normalize(test_features, means, vars);
    
    % Get the proper labels for the test data.
    for i=1:length(test_boxes);
        for j=1:length(locations);
            if Box_Contains(test_boxes(i,:), locations(j,:));
                test_labels(i,1) = classes(j,1);
            end;
        end;
    end;
    
    % Predict the test data.
    D = dist2(test_features, train_features);
    [D_sorted, D_index] = sort(D, 2);
    num_correct_test = 0;
    num_total_test = length(test_features);
    
    if experiments('svm');
        svmmodel = fitcecoc(train_features, train_labels);
    end;
    
    result = [];
    
    for i=1:length(test_features);        
        if experiments('topk');
            % Adding top 9 nearest helps a bit.
            top = train_labels(D_index(i,1:9));
            majority = mode(top);
            prediction = majority;
        elseif experiments('svm');
            % Using SVM (Improves)
            prediction = predict(svmmodel, test_features(i,:));
        else
            % Default chooses closest match.
            prediction = train_labels(D_index(i,1));
        end;

        str = strcat('(' , letters(test_labels(i,1)) , ',', letters(prediction) , ')');
        text(test_boxes(i,4), test_boxes(i,2), str);
        if (prediction == test_labels(i,1));
            num_correct_test = num_correct_test + 1;
        end;
        result = [result; prediction];
    end;
    title('Test Results');
    
    % Predict the training data by selecting the second best match.
    num_correct_train = 0;
    num_total_train = 0;
    for i=1:length(letters);
        letter = letters(i);
        filename = strcat('images/', letter, '.bmp');
        [train_file_features, train_file_boxes] = OCR_Extract_Features(show_train_images, filename);
        [train_file_features] = Normalize(train_file_features, means, vars);
        num_total_train = num_total_train + length(train_file_features);
        D_train = dist2(train_file_features, train_features);
        [D_train_sorted, D_train_index] = sort(D_train, 2);
        actual = train_labels(D_train_index(:,1));
        predicted = train_labels(D_train_index(:,2));
        if (show_train_images);
            title(strcat('Training results on letter=', letter));
            for j=1:length(actual);
                x = train_file_boxes(j,4);
                y = train_file_boxes(j,2);
                str = strcat('(' , letters(actual(j)) , ',', letters(predicted(j)), ')');
                text(x, y, str);
            end;
        end;
        num_incorrect = nnz(actual - predicted);
        %disp(strcat('Percent correct for letter ', letter));
        %percent = (length(train_file_features) - num_incorrect) / length(train_file_features);
        %disp(percent);
        num_correct_train = num_correct_train + length(train_file_features) - num_incorrect;
    end;
    
    
    fprintf('Testing recognition rate\n');
    fprintf('%d out of %d correct = %.3f percent\n', num_correct_test, num_total_test, 100 * num_correct_test/num_total_test);
    
    fprintf('Training recognition rates\n');
    fprintf('%d out of %d correct = %.3f percent\n', num_correct_train, num_total_train, 100 * num_correct_train/num_total_train);
    
    %{
    fprintf('Running experiments:\n');
    keySet = keys(experiments);
    valueSet = values(experiments);
    for i=1:length(keySet);
        fprintf('%s = %d\n', keySet{1,i}, valueSet{1,i});
    end;
    %}
