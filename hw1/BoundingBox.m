function [features, boxes] = BoundingBox(L, im, showplot)
    experiments = GetExperiments();
    Nc=max(max(L));
    if showplot == 1;
        figure;
        colormap gray;
        imagesc(~L);
        hold on;
    end;
    
    features = [];
    boxes = [];
    
    for i=1:Nc;
     [r,c]=find(L==i);
     maxr=max(r);
     minr=min(r);
     maxc=max(c);
     minc=min(c);
     
     width = maxr - minr;
     height = maxc - minc;
     minDimension = 10;
     maxDimension = 100;
     if width < minDimension || height < minDimension || width > maxDimension || height > maxDimension;
         continue;
     end;
     
     boxes = [boxes; minr, maxr, minc, maxc];
     
     if showplot == 1;
         rectangle('Position',[minc,minr,maxc-minc+1,maxr-minr+1], 'EdgeColor','b');
     end;
     
     
     area = ((maxc-minc) * (maxr - minr));

     % Get the horizontal symmetry.
     half = floor((maxc - minc)/2);
     left = im(minr:maxr,minc:minc+half);
     % Flip right matrix to line up.
     right = fliplr(im(minr:maxr,maxc-half:maxc));
     horizontal_diff = sum(sum(abs(left - right))) / area;
     
     % Get the vertical symmetry.
     half = floor((maxr - minr)/2);
     top = im(minr:minr+half,minc:maxc);
     % Flip bottom matrix to line up.
     bottom = flipud(im(maxr-half:maxr,minc:maxc));
     vertical_diff = sum(sum(abs(top - bottom))) / area;
     
     % Check for holes with components (degrades).
     component_count = max(max(bwlabel(im(minr-1:maxr+1,minc-1:maxc+1))));
     
     % Check for height/width radio. (degrades).
     ratio = (maxr-minr)/(maxc-minc);
     
     cim = im(minr:maxr,minc:maxc);
     [centroid, theta, roundness, inmo] = moments(cim, 1);
     row = [theta, roundness, inmo];
     
     if experiments('symmetry');
        row = horzcat(row, [horizontal_diff, vertical_diff]);
     end;
     
     if experiments('dimension_ratio');
         row = horzcat(row, [ratio]);
     end;
     
     if experiments('component_count');
         row = horzcat(row, [component_count]);
     end;
     
     if experiments('normalized_centroid');
        row = horzcat(row, centroid/area);
     end;
     
     features = [features; row];
    end
    
    if (showplot);
        hold off;
    end;