% Displays confusion matrix for second closest letter in feature matrix.
[features, labels] = OCR_Make_Model();
[features, means, vars] = Normalize(features);
D = dist2(features, features);
[D_sorted, D_index] = sort(D, 2);
actual = labels(D_index(:,1));
predicted = labels(D_index(:,2));
letters = GetCharLabels();
conf = ConfusionMatrix(actual, predicted, length(letters));
imagesc(conf);
title('Confusion Matrix');
ax = gca;
ax.XTick = [1:length(letters)];
ax.XTickLabels = letters;
ax.YTick = [1:length(letters)];
ax.YTickLabels = letters;