part2;

% Augment the plane with clip art.

disp('Because my RUID last four sum to 20, I used image 2 since there''s no image 0');
AugmentReality('resources/images2.png', H_correct_2, 1);
AugmentReality('resources/images9.png', H_correct_9, 1);
AugmentReality('resources/images12.png', H_correct_12, 1);
AugmentReality('resources/images20.png', H_correct_20, 1);


% Augment the plane with a cube.

cubePoints = [
    1,1,0,1;
    30,1,0,1;
    30,30,0,1;
    1,30,0,1;
    1,1,-30,1;
    30,1,-30,1;
    30,30,-30,1;
    1,30,-30,1];

cubeEdges = [
    % Bottom face.
    1,2;
    2,3;
    3,4;
    4,1;
    
    % Top face.
    5,6;
    6,7;
    7,8;
    8,5;
    
    % Sides.
    1,5;
    2,6;
    3,7;
    4,8;
    
    ];

disp('The points of the cube are');
disp(cubePoints);

% This homography will map points with a z coordinate.
H2_3D = A_correct * horzcat(R_correct_2, t_correct_2);
AugmentRealityMesh('resources/images2.png', H2_3D, cubePoints, cubeEdges, 1, 1);

H9_3D = A_correct * horzcat(R_correct_9, t_correct_9);
AugmentRealityMesh('resources/images9.png', H9_3D, cubePoints, cubeEdges, 1, 1);

H12_3D = A_correct * horzcat(R_correct_12, t_correct_12);
AugmentRealityMesh('resources/images12.png', H12_3D, cubePoints, cubeEdges, 1, -1);

H20_3D = A_correct * horzcat(R_correct_20, t_correct_20);
AugmentRealityMesh('resources/images20.png', H20_3D, cubePoints, cubeEdges, 1, -1);