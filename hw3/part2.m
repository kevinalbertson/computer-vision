im2Corners = [
    91 72 1;
    522 80 1;
    534 420 1;
    65 410 1
];

im9Corners = [
    136 22 1;
    552 77 1;
    567 394 1;
    130 420 1;
];

im12Corners = [
    111 89 1;
    508 24 1;
    526 411 1;
    103 394 1
];

im20Corners = [
    175 82 1;
    512 80 1;
    576 271 1;
    124 272 1;
];

gridWorldCorners = [
    0 210 1;
    270 210 1;
    270 0 1;
    0 0 1;
];

% Compute homographies, normalize them.
H2 = homography2d(gridWorldCorners', im2Corners');
H2 = H2 / H2(3,3);
H9 = homography2d(gridWorldCorners', im9Corners');
H9 = H9 / H9(3,3);
H12 = homography2d(gridWorldCorners', im12Corners');
H12 = H12 / H12(3,3);
H20 = homography2d(gridWorldCorners', im20Corners');
H20 = H20 / H20(3,3);

VMat = [getEq2_25(H2); getEq2_25(H9); getEq2_25(H12); getEq2_25(H20)];
[A, B, lambda] = ComputeIntrinsic(VMat);

% Normalize C
C = inv(A)' * inv(A);
C = C / C(3,3);

[im2_r, im2_r_fixed, im2_t] = ComputeExtrinsic(A, H2);
[im9_r, im9_r_fixed, im9_t] = ComputeExtrinsic(A, H9);
[im12_r, im12_r_fixed, im12_t] = ComputeExtrinsic(A, H12);
[im20_r, im20_r_fixed, im20_t] = ComputeExtrinsic(A, H20);


% Display images with projected image corners.
ApproximateLocations(H2, 'resources/images2.png', 0);
ApproximateLocations(H9, 'resources/images9.png', 0);
ApproximateLocations(H12, 'resources/images12.png', 0);
ApproximateLocations(H20, 'resources/images20.png', 0);


[p_approx_2, gridPoints] = ApproximateLocations(H2, 'resources/images2.png', 0);
p_correct_2 = HarrisCorners('resources/images2.png', p_approx_2, 0);
H_correct_2 = homography2d(gridPoints', p_correct_2');

[p_approx_9, gridPoints] = ApproximateLocations(H9, 'resources/images9.png', 0);
p_correct_9 = HarrisCorners('resources/images9.png', p_approx_9, 0);
H_correct_9 = homography2d(gridPoints', p_correct_9');

[p_approx_12, gridPoints] = ApproximateLocations(H12, 'resources/images12.png', 0);
p_correct_12 = HarrisCorners('resources/images12.png', p_approx_12, 0);
H_correct_12 = homography2d(gridPoints', p_correct_12');

[p_approx_20, gridPoints] = ApproximateLocations(H20, 'resources/images20.png', 0);
p_correct_20 = HarrisCorners('resources/images20.png', p_approx_20, 0);
H_correct_20 = homography2d(gridPoints', p_correct_20');


VMat_correct = [getEq2_25(H_correct_2); getEq2_25(H_correct_9); getEq2_25(H_correct_12); getEq2_25(H_correct_20)];

[A_correct, B_correct, lambda_correct] = ComputeIntrinsic(VMat_correct);

% Compute extrinsic parameters for each image.
[R_correct_2, R_fixed_correct_2, t_correct_2] = ComputeExtrinsic(A_correct, H_correct_2);
[R_correct_9, R_fixed_correct_9, t_correct_9] = ComputeExtrinsic(A_correct, H_correct_9);
[R_correct_12, R_fixed_correct_12, t_correct_12] = ComputeExtrinsic(A_correct, H_correct_12);
[R_correct_20, R_fixed_correct_20, t_correct_20] = ComputeExtrinsic(A_correct, H_correct_20);

% Compute errors
[err_reprojection_2] = ComputeReprojectionError(p_correct_2, H_correct_2);
[err_reprojection_9] = ComputeReprojectionError(p_correct_9, H_correct_9);
[err_reprojection_12] = ComputeReprojectionError(p_correct_12, H_correct_12);
[err_reprojection_20] = ComputeReprojectionError(p_correct_20, H_correct_20);

