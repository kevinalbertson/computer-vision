function [p_approx, worldPoints] = ApproximateLocations(H, filename, plotGraph)
    p_approx = [];
    worldPoints = [];
    for i=0:9;
        for j=0:7;
            worldPoints = [worldPoints; i * 30, j * 30, 1];
        end;
    end;
    
    if (plotGraph)
        figure;
        im = imread(filename);
        imagesc(im);
        title('Figure 1 : Projected grid corners');
        hold on;
    end
    
    for i=1:size(worldPoints);
        imagePoint = H * worldPoints(i,:)';
        imagePoint = imagePoint / imagePoint(3);
        if (plotGraph)
            plot(imagePoint(1), imagePoint(2), 'o');
        end;
        p_approx = [p_approx; imagePoint'];
    end
    
    if (plotGraph)
        hold off;
    end;