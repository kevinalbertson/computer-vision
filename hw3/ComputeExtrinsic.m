function [r, r_fixed, t] = ComputeExtrinsic(A, H)
    AInv = inv(A);
    lambda = 1 / norm(AInv * H(:,1));
    r1 = lambda * AInv * H(:,1);
    r2 = lambda * AInv * H(:,2);
    r3 = cross(r1, r2);
    r = horzcat(r1, r2, r3);
    [U, S, V] = svd(r);
    r_fixed = U * V'; 
    t = lambda * AInv * H(:,3);
end