function [] = AugmentReality(filename, H, showImage);
    % I chose two since my RUID sums to 20 and there is no 0 image.
    clipart = imresize(rgb2gray(imread('resources/clipart/2.jpg', 'jpg')), .4);

    im = rgb2gray(imread(filename));

    % Inverse maps image points to "world points".
    HInv = inv(H);
    for i=1:size(im, 1);
        for j=1:size(im, 2);
            % Given inverse, multiply by [u,v,1].
            worldPoint = HInv * [j,i,1]';
            worldPoint = worldPoint / worldPoint(3);
            worldPoint = floor(worldPoint);
            worldPoint(2) = size(clipart, 2) - worldPoint(2) + 1;
            if (worldPoint(1) <= 0 || worldPoint(2) <= 0);
                continue;
            elseif (worldPoint(1) > size(clipart, 2) || worldPoint(2) > size(clipart, 1));
                continue;
            end;
            clipartPixel = clipart(worldPoint(2), worldPoint(1));
            % Apply a small threshold to remove mostly white pixels.
            if (clipartPixel >= [245, 245, 245]);
                continue;
            end;
            im(i,j) = clipartPixel;
        end;
    end;
    if (showImage);
        figure;
        imshow(im);
        title(strcat(['Augmented plane for ', filename]));
    end;
end