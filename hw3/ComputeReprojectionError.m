function [err] = ComputeReprojectionError(p_correct, H)
    [p_approx, gridPoints] = ApproximateLocations(H, '', 0);
    errMat = dist2(p_approx, p_correct);
    err = [];
    for i = 1:size(p_approx);
        err = [err; errMat(i,i)];
    end;
end