im = ones(600);
imPoints = [
    422 323 1;
    178 323 1;
    118 483 1; 
    482 483 1; 
    438 73 1; 
    162 73 1; 
    78 117 1; 
    522 117 1];

worldPoints = [
    2 2 2 1;
    -2 2 2 1;
    -2 2 -2 1;
    2 2 -2 1;
    2 -2 2 1;
    -2 -2 2 1;
    -2 -2 -2 1;
    2 -2 -2 1;
    ];

iptsetpref('ImshowAxesVisible','on');

figure;
imshow(im);
hold on;
for i=1:8;
    plot(imPoints(i,1), imPoints(i,2), 'o');
end;
title('Cube points');
hold off;

    
P = [];
for i=1:8;
    rows = getRowP(worldPoints(i,:)', imPoints(i,:)');
    P = [P; rows];
end;

disp('Matrix P is');
disp(P);

% Compute M
[U, S, V] = svd(P);
m = V(:,end);
M = [m(1:4)'; m(5:8)'; m(9:12)'];
proj = M * worldPoints(2,:)';

disp('Matrix M is');
disp(M);

% Compute translation vector.
[U, S, V] = svd(M);
translationHomo = V(:,end);
translation = translationHomo(1:3) / translationHomo(4);

disp('Euclidean coordinates of translation are');
disp(translation);

Mp = M(1:3,1:3) / M(3,3);
disp('M'' is');
disp(Mp);

% Compute rotation matrix for x axis.
m33 = Mp(3,3);
m32 = Mp(3,2);
cosThetaX = m33/sqrt(m33^2 + m32^2);
sinThetaX = -m32/sqrt(m33^2 + m32^2);
thetaX = acos(cosThetaX);
Rx = [1,0,0;
    0,cosThetaX, -sinThetaX;
    0,sinThetaX, cosThetaX];
N = Mp * Rx;

disp('Rx is');
disp(Rx);

disp('Theta_x is');
disp(thetaX);

disp('N is');
disp(N);

% Compute rotation matrix for z axis.
n22 = N(2,2);
n21 = N(2,1);
cosThetaZ = n22/sqrt(n21^2 + n22^2);
sinThetaZ = -n21/sqrt(n21^2 + n22^2);
thetaZ = acos(cosThetaZ) * 180 / pi;

Rz = [cosThetaZ, -sinThetaZ, 0;
    sinThetaZ, cosThetaZ, 0;
    0, 0, 1];

disp('Rz is');
disp(Rz);

disp('Theta_z is');
disp(thetaZ);

K = N * Rz;
K = K / K(3,3);
centerU = K(1,3);
centerV = K(2,3);
fx = K(1,1);
fy = K(2,2);

disp('K is');
disp(K);

disp('The center of the camera is');
disp('u =');
disp(centerU);
disp('v =');
disp(centerV);

disp('The focal lengths of the camera are');
disp('fx =');
disp(fx);
disp('fy =');
disp(fy);