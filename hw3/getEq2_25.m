function [vMatrix] = getEq2_25(H)
    function [vij] = computeVij(i, j)
        vij = [
            H(1,i)*H(1,j);
            H(1,i)*H(2,j) + H(2,i)*H(1,j);
            H(2,i)*H(j,2);
            H(3,i)*H(1,j) + H(1,i)*H(3,j);
            H(3,i)*H(2,j) + H(2,i)*H(3,j);
            H(3,i)*H(3,j)
            ]';
    end
    vMatrix = [
        computeVij(1, 2);
        computeVij(1, 1) - computeVij(2, 2)
        ];
end