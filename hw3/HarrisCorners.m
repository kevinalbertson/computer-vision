function [p_correct] = HarrisCorners(filename, p_approx, drawGraph)
    im = imread(filename);
    sigma = 2;
    thresh = 500;
    radius = 2;
    
    [cim, r, c, rsubp, csubp] = harris(rgb2gray(im), sigma, thresh, radius, 0);
    
    if drawGraph;
        figure;
        imagesc(im);
        hold on;
    end;
    
    cornerPoints = horzcat(csubp, rsubp, ones(size(csubp)));
    
    distMatrix = dist2(p_approx, cornerPoints);
    [~, sortedIndices] = sort(distMatrix, 2);
    p_correct = cornerPoints(sortedIndices(:, 1),:);
    if drawGraph;
        for i=1:size(p_correct);
            plot(p_correct(i,1), p_correct(i,2), 'o');
        end;
    end;
    
    if drawGraph;
        title('Figure 3: Grid Points');
        hold off;
    end;
end