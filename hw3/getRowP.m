% This function returns two rows in matrix P
% worldCoord should be homogeneous 4x1 vector
% imCoord should be homogeneous 3x1 vector
function [rows] = getRowP(worldCoord, imCoord)
    u = imCoord(1) / imCoord(3);
    v = imCoord(2) / imCoord(3);
    row1 = horzcat(worldCoord', zeros(1,4), -u * worldCoord');
    row2 = horzcat(zeros(1,4), worldCoord', -v * worldCoord');
    rows = [row1;row2];
end