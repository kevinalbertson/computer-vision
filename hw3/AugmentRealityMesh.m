function [] = AugmentRealityMesh(filename, H, meshPoints, meshEdges, showImage, zFlip);
    im = rgb2gray(imread(filename));
    
    function [imagePoint] = projectPoint(meshPoint)
        meshPoint(3) = meshPoint(3) * zFlip;
        imagePoint = H * meshPoint';
        imagePoint = imagePoint / imagePoint(3);
        imagePoint = floor(imagePoint);
    end;
    if showImage;
        figure;
        imshow(im);
        hold on;
    end;
    
    for i=1:size(meshEdges);
        edge = meshEdges(i,:);
        indexA = edge(1);
        indexB = edge(2);
        pointA = projectPoint(meshPoints(indexA,:));
        pointB = projectPoint(meshPoints(indexB,:));
        if (showImage);
            plot(pointA(1), pointA(2), 'o');
            plot(pointB(1), pointB(2), 'o');
            line([pointA(1), pointB(1)], [pointA(2), pointB(2)], 'Color', 'r', 'LineWidth', 1);
        end;
    end;
    
    if (showImage);
        hold off;
        title(strcat(['Augmented with mesh for ', filename]));
    end;
end