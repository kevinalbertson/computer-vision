function [A, B, lambda] = ComputeIntrinsic (VMat)
    [~, ~, V] = svd(VMat);
    b = V(:,end);
    B11 = b(1);
    B12 = b(2);
    B22 = b(3);
    B13 = b(4);
    B23 = b(5);
    B33 = b(6);
    v0 = (B12 * B13 - B11 * B23) / (B11 * B22 - B12 ^ 2);
    lambda = B33 - (B13^2 + v0 * (B12*B13 - B11*B23)) / B11;
    alpha = sqrt(lambda/B11);
    beta = sqrt(lambda*B11 / (B11*B22 - B12^2));
    gamma = -1 * B12*(alpha^2)*beta/lambda;
    u0 = gamma * v0 / alpha - B13 * (alpha ^ 2) / lambda;

    B = [B11, B12, B13;
        B12, B22, B23;
        B13, B23, B33];

    A = [alpha, gamma, u0;
        0, beta, v0;
        0, 0, 1;];
end