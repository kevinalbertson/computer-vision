part2;

meshObj = read_wobj('examples/example10.obj');
vertices = meshObj.vertices;
vertices = vertices;
% Rotate each vertex by 90 degrees in the x axis.
rotX = [
    1 0 0;
    0 0 1;
    0 -1 0
    ];
for i=1:size(vertices);
    vertices(i,:) = rotX * vertices(i,:)' + [30, 30, -50]';
end;

vertices = horzcat(vertices, ones(size(vertices, 1), 1));
faces = meshObj.objects(3).data.vertices;
edges = [];
for i=1:size(faces);
    edges = [edges; faces(i,1), faces(i,2)];
    edges = [edges; faces(i,2), faces(i,3)];
end;

H2_3D = A_correct * horzcat(R_correct_2, t_correct_2);
AugmentRealityMesh('resources/images2.png', H2_3D, vertices, edges, 1, 1);

H9_3D = A_correct * horzcat(R_correct_9, t_correct_9);
AugmentRealityMesh('resources/images9.png', H9_3D, vertices, edges, 1, 1);

% For whatever reason, the z-axis is flipped on this one.
H12_3D = A_correct * horzcat(R_correct_12, t_correct_12);
AugmentRealityMesh('resources/images12.png', H12_3D, vertices, edges, 1, -1);

H20_3D = A_correct * horzcat(R_correct_20, t_correct_20);
AugmentRealityMesh('resources/images20.png', H20_3D, vertices, edges, 1, -1);
