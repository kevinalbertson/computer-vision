% Given the path to an image, fills in the green [0 255 0] areas with
% synthesized textures from the image.
% The image is assumed to be in color with the areas needed
% to be filled in [0 255 0].
% Example usage: RunRemoval('images/removals/3_s.png')
%
% To change the window size, see GetSettings.m
%

function [] = RunRemoval(ImageFilePath)
    colorIm = imread(ImageFilePath);
    
    % The mask will contain 1's where the image is empty.
    mask = zeros(size(colorIm, 1), size(colorIm, 2));

    for i=1:size(colorIm, 1);
        for j=1:size(colorIm, 2);
            if (colorIm(i,j,1) == 0 && colorIm(i,j,2) == 255 && colorIm(i,j,3) == 0);
                mask(i,j) = 1;
            end;
        end;
    end;

    blackAndWhite = double(rgb2gray(colorIm)) / 255;
    % Subtract off anywhere there was a filling area.
    blackAndWhite = blackAndWhite - (mask .* blackAndWhite);
    % Add back on -1.
    blackAndWhite = blackAndWhite + (mask * -1);
    Synthesize(blackAndWhite, blackAndWhite);
end