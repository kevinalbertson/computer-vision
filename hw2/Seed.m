function [TargetImage] = Seed(SampleImage, TargetWidth)
    % -1 represents an unset pixel.
    TargetImage = ones(TargetWidth) * -1;

    % First seed the target image with a random 3x3 window.
    [nrows, ncols] = size(SampleImage);
    row = randi(nrows - 2);
    col = randi(ncols - 2);

    Sample = SampleImage(row:(row+2),col:(col+2));

    row = TargetWidth / 2 - 1;
    col = TargetWidth / 2 - 1;

    TargetImage(row:row+2, col:col+2) = Sample;
end