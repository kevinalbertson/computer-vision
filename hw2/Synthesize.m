function [] = Synthesize(SampleImage, TargetImage)
    tic;
    StartingAmount = sum(sum(TargetImage == -1));
    UnfilledNeighbors = GetUnfilledNeighbors(TargetImage);
    TotalFilled = 0;
    Settings = GetSettings();
    WindowSize = Settings('window_size');
    GaussMask = fspecial('gaussian', [WindowSize, WindowSize], WindowSize / 6.4);
    BlockMatrix = im2col(SampleImage, [WindowSize, WindowSize], 'sliding');
    MaxErrorThreshold = .3;
    Progress = 0;
    fprintf('Here we go.\n');
    while size(UnfilledNeighbors) > 0;
        Progress = 0;
        for pi=1:size(UnfilledNeighbors, 1);
            Pixel = UnfilledNeighbors(pi,:);
            [BestMatches] = GetBestMatches(Pixel, TargetImage, SampleImage, BlockMatrix, GaussMask);
            % Choose a random pixel from the best matches to place.
            Match = BestMatches(randi(size(BestMatches,1)),:);
            if (Match(3) < MaxErrorThreshold)
                TargetImage(Pixel(1), Pixel(2)) = SampleImage(Match(1), Match(2));
                Progress = 1;
            end;
        end;
        if (Progress == 0)
            fprintf('Max increasing');
            MaxErrorThreshold = MaxErrorThreshold * 1.1;
        end;
        TotalFilled = TotalFilled + size(UnfilledNeighbors,1);
        UnfilledNeighbors = GetUnfilledNeighbors(TargetImage);
        fprintf('Only %d to go!\n', StartingAmount - TotalFilled);
    end;

    figure();
    imshow(TargetImage .* double(TargetImage > -1));

    fprintf('Took time %d.\n', toc);
end
