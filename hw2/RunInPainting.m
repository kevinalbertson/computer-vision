% Given the path to an image, fills in the black areas with
% synthesized textures from the image.
% The image is assumed to be greyscale with the areas needed
% to be filled in pure black.
% Example usage: RunInPainting('images/test_im2.bmp')
%
% To change the window size, see GetSettings.m
%

function [] = RunInPainting(ImageFilePath)
    SampleImage = EmptyFilter(ReadImage(ImageFilePath));
    Synthesize(SampleImage, SampleImage);
end