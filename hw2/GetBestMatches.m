% SampleImage and TargetImage may both have "missing" pixels denoted by the
% value -1.

function [BestMatches] = GetBestMatches(Pixel, TargetImage, SampleImage, BlockMatrix, GaussMask)
    Settings = GetSettings();
    WindowSize = Settings('window_size');
    row = Pixel(1);
    col = Pixel(2);
    halfWindow = floor(WindowSize / 2);

    % Template is the window of the target image.

    Template = ones(WindowSize) * -1;
    for i=-halfWindow:halfWindow;
        for j=-halfWindow:halfWindow;
            TargetRow = row + i;
            TargetCol = col + j;
            [nRows, nCols] = size(TargetImage);
            if (TargetRow < 1 || TargetCol < 1 || TargetRow > nRows || TargetCol > nCols);
                continue;
            end;
            Template(i + halfWindow + 1,j + halfWindow + 1) = TargetImage(TargetRow, TargetCol);
        end;
    end;

    % ValidMask contains 1 if filled, 0 otherwise.
    ValidMask = double(Template ~= -1);

    TotWeight = sum(sum(GaussMask .* ValidMask));
    SSD = zeros(size(SampleImage));
    
    % This is the original naive algorithm.
    %{
        for i=1+halfWindow:size(SampleImage,1)-halfWindow-1;
            for j=1+halfWindow:size(SampleImage,2)-halfWindow-1;
                for ii=-halfWindow:halfWindow
                    si = i + ii;
                    ti = halfWindow + 1 + ii;
                    for jj=-halfWindow:halfWindow;
                        % TODO: double check the logic here.
                        sj = j + jj;
                        tj = halfWindow + 1 + jj;
                        dist = (Template(ti,tj) - SampleImage(si, sj)) ^ 2;
                        SSD(i,j) = SSD(i,j) + dist * ValidMask(ti,tj) * GaussMask(ti,tj);
                    end;
                end;
                SSD(i,j) = SSD(i,j) / TotWeight;
            end;
        end;
    %}
    
    
    MaskVector = GaussMask .* ValidMask;
    MaskVector = MaskVector(:);
    MaskVector = MaskVector';
    % Doing a matrix multiplication by the mask vector as a row vector does
    % the summation for us.
    ReshapedTemplate = repmat(im2col(Template, [WindowSize, WindowSize], 'sliding'), 1, size(BlockMatrix, 2));
    %ReshapedMask = repmat(im2col(ValidMask .* GaussMask, [WindowSize, WindowSize], 'sliding'), 1, size(BlockMatrix, 2));
    Output = MaskVector * (BlockMatrix - ReshapedTemplate) .^ 2;
    
    % If it is possible for the sample to have missing pixels, ensure that
    % these don't interfere.
    if (Settings('sample_has_missing') == 1)
        % Also check to see if the Sample Image is missing pixels in the
        % block, ignore it.
        MinInBlocks = min(BlockMatrix);
        for i=1:size(MinInBlocks, 2);
            if (MinInBlocks(i) == -1);
                %fprintf('Sample image missing pixel, removing block\n');
                Output(i) = Inf();
            end;
        end;
    end;
    % col2im returns the "cropped" values, i.e. only the values within the
    % sliding neighborhood of the sample image.
    SSD = col2im(Output, [WindowSize, WindowSize], size(SampleImage), 'sliding');

    Closest = min(min(SSD));
    MinimumValue = Closest * (1+Settings('err_threshold'));
    BestMatches = [];

    for i=1:size(SSD, 1);
        for j=1:size(SSD, 2);
            if (SSD(i,j) <= MinimumValue);
                BestMatches = [BestMatches; i+halfWindow, j+halfWindow, SSD(i,j)];
            end;
        end;
    end;
end