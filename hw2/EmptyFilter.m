function [OutputImage] = EmptyFilter(InputImage)
    function [Output] = BlackFilter(Input)
        if (Input == 0);
            Output = -1;
        else
            Output = Input;
        end;
    end
    OutputImage = arrayfun(@BlackFilter, InputImage);
end