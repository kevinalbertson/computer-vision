function [Image] = ReadImage(ImagePath)
    Image = double(imread(ImagePath)) ./ 255;
end