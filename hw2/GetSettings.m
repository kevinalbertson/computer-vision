function [Settings] = GetSettings()
    Settings = containers.Map();
    Settings('window_size') = 5;
    Settings('err_threshold') = .1;
    
    % If this is true, then the sample may have missing pixels.
    % To optimize image synthesis set this to 0.
    % It is necessary to be 1 for image inpainting.
    Settings('sample_has_missing') = 1;
end