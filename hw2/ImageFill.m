function[] = ImageFill(FilledImage)
% Read image as black and white.
colorIm = imread(FilledImage);
mask = zeros(size(colorIm, 1), size(colorIm, 2));

for i=1:size(colorIm, 1);
    for j=1:size(colorIm, 2);
        if (colorIm(i,j,1) == 0 && colorIm(i,j,2) == 255 && colorIm(i,j,3) == 0);
            mask(i,j) = 1;
        end;
    end;
end;

blackAndWhite = double(rgb2gray(colorIm)) / 255;
% Subtract off anywhere there was a filling area.
blackAndWhite = blackAndWhite - (mask .* blackAndWhite);
% Add back on -1.
blackAndWhite = blackAndWhite + (mask * -1);
Synthesize(blackAndWhite, blackAndWhite);
end