function [ UnfilledNeighbors ] = GetUnfilledNeighbors( TargetImage )
    Settings = GetSettings();
    WindowSize = Settings('window_size');
    
    % FilledArea has 0 if unfilled, 1 if filled.
    % It must be numeric (not logical) type to work
    % correctly with imfilter.
    FilledArea = double(TargetImage ~= -1);
    
    [UnfilledRow, UnfilledCol] = find(FilledArea == 0);
    UnfilledNeighbors = [];
    UnfilledNeighborCounts = [];
    
    % Compute the number of filled neighbors in each and return a n x 2
    % matrix with the coordinates.
    % disp(TargetImage);
    
    
    filter = ones(WindowSize, WindowSize);
    filter(ceil(WindowSize/2), ceil(WindowSize/2)) = 0;
    
    % Note, that the entries also include already filled elements, so
    % we need to ignore those.
    FilledNeighborsCounts = imfilter(FilledArea, filter);
    
    for i=1:size(UnfilledRow)
        row = UnfilledRow(i);
        col = UnfilledCol(i);
        % Ignore pixels without any neighbor.
        if (FilledNeighborsCounts(row,col) == 0);
            continue;
        end;
        UnfilledNeighbors = [UnfilledNeighbors; row, col, FilledNeighborsCounts(row,col)];
        UnfilledNeighborCounts = [UnfilledNeighborCounts; FilledNeighborsCounts(row,col)];
    end;
    
    % Sort UnfilledNeighborCounts and then rearrange UnfilledNeighbors
    % to be sorted.
    [Sorted, SortedIndexes] = sort(UnfilledNeighborCounts, 'descend');
    UnfilledNeighbors = UnfilledNeighbors(SortedIndexes,:);
    
end

