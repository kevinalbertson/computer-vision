% Given the path to an image, generates a 200x200
% synthesized texture and plots it.
% The image is assumed to be greyscale.
% Example usage: RunSynthesis('images/T1.gif')
%
% To change the window size, see GetSettings.m
%

function [] = RunSynthesis(ImageFilePath)
    SampleImage = ReadImage(ImageFilePath);
    TargetImage = Seed(SampleImage, 200);
    Synthesize(SampleImage, TargetImage);
end